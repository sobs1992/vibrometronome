#include <stdint.h>
#include "fontStructures.h"

// Font data for Segoe Print 10pt
extern const uint8_t segoePrint_10ptBitmaps[];
extern const FONT_INFO segoePrint_10ptFontInfo;
extern const FONT_CHAR_INFO segoePrint_10ptDescriptors[];

