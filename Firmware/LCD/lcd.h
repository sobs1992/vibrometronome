#include <stdint.h>
#include <stdlib.h>
#include "..\i2c\i2c.h"
#include "font_20p.h"
#include "font_10p.h"

#define LCD_ID      0x78

#define LABEL_N     2

typedef struct {
    uint8_t x;
    uint8_t y;
    FONT_INFO font;
    uint8_t text_width;
} _label;

extern const uint8_t play[];
extern const uint8_t stop[];
extern const uint8_t beat_0[];
extern const uint8_t beat_1[];
extern const uint8_t plus_icon[];
extern const uint8_t vibro_icon[];
extern const uint8_t vibro_power_icon[];
extern const uint8_t time_power_icon[];
extern const uint8_t time_icon[];

void LCD_Init();
void LCD_Fill(uint8_t sx, uint8_t sy, uint8_t ex, uint8_t ey, uint8_t color);
uint8_t LCD_Char(uint8_t x, uint8_t y, FONT_INFO font, uint8_t sym);
uint8_t LCD_Text(uint8_t x, uint8_t y, FONT_INFO font, uint8_t *text);
void LCD_Bitmap(uint8_t x, uint8_t y, uint8_t sx, uint8_t sy, const uint8_t *bitmap);
void LCD_BitmapCol(uint8_t x, uint8_t y, uint8_t bitmap_col);
void LCD_Draw_Battery(uint8_t x, uint8_t y, uint8_t level);
uint8_t GetTextLen(FONT_INFO font, uint8_t *text);
uint8_t LCD_Create_Label(uint8_t x, uint8_t y, FONT_INFO font, uint8_t *text);
void LCD_Label_Text(uint8_t id, uint8_t *text);
void LCD_Label_Num(uint8_t id, uint16_t num);