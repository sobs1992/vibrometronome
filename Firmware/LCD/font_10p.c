#include "font_10p.h"

// 
//  Font data for Segoe Print 10pt
// 

// Character bitmaps for Segoe Print 10pt
// Height = 15 bit
const uint8_t segoePrint_10ptBitmaps[] = 
{
	// @0 ' ' (2 pixels wide)
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	0x00, 0x00, 
	0x00, 0x00, 

	// @4 '!' (3 pixels wide)
	//    
	//    
	//  ##
	//  ##
	//  ##
	//  # 
	// ## 
	// ## 
	//    
	// #  
	// #  
	//    
	//    
	//    
	//    
	0xC0, 0xFC, 0x1C, 
	0x06, 0x00, 0x00, 

	// @10 '"' (4 pixels wide)
	//     
	//     
	// #  #
	// #  #
	// # ##
	// # # 
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	0x3C, 0x00, 0x30, 0x1C, 
	0x00, 0x00, 0x00, 0x00, 

	// @18 '#' (10 pixels wide)
	//           
	//           
	//        #  
	//     #  #  
	//     #  #  
	//    #######
	// ####  #   
	//   #  #### 
	// ######    
	//   #  #    
	//   #  #    
	//           
	//           
	//           
	//           
	0x40, 0x40, 0xC0, 0x60, 0x38, 0xA0, 0xE0, 0xBC, 0xA0, 0x20, 
	0x01, 0x01, 0x07, 0x01, 0x01, 0x07, 0x00, 0x00, 0x00, 0x00, 

	// @38 '$' (7 pixels wide)
	//        
	//     #  
	//     #  
	//   #####
	//  #  #  
	//   ##   
	//    ##  
	//    # # 
	//    ##  
	// ####   
	//    #   
	//    #   
	//        
	//        
	//        
	0x00, 0x10, 0x28, 0xE8, 0x5E, 0x88, 0x08, 
	0x02, 0x02, 0x02, 0x0F, 0x01, 0x00, 0x00, 

	// @52 '%' (8 pixels wide)
	//         
	//         
	//  ###### 
	// # #  #  
	// # # #   
	// ##  #   
	//    #    
	//   #  ## 
	//   # #  #
	//  #  #  #
	//  #  ### 
	//         
	//         
	//         
	//         
	0x38, 0x24, 0x9C, 0x44, 0x34, 0x8C, 0x84, 0x00, 
	0x00, 0x06, 0x01, 0x00, 0x07, 0x04, 0x04, 0x03, 

	// @68 '&' (11 pixels wide)
	//            
	//     ##     
	//    # #     
	//    # #     
	//    # #     
	//    ##      
	//  ###  #####
	// ##  #  #   
	// #    # #   
	// #     #    
	//  ##### ####
	//            
	//            
	//            
	//            
	0x80, 0xC0, 0x40, 0x7C, 0xA2, 0x1E, 0x40, 0xC0, 0x40, 0x40, 0x40, 
	0x03, 0x04, 0x04, 0x04, 0x04, 0x05, 0x02, 0x05, 0x04, 0x04, 0x04, 

	// @90 ''' (2 pixels wide)
	//   
	//   
	//  #
	//  #
	// # 
	// # 
	// # 
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	0x70, 0x0C, 
	0x00, 0x00, 

	// @94 '(' (5 pixels wide)
	//      
	//     #
	//   ## 
	//   #  
	//  #   
	//  #   
	// #    
	// #    
	// #    
	// #    
	// ##   
	//  #   
	//  ##  
	//   ## 
	//      
	0xC0, 0x30, 0x0C, 0x04, 0x02, 
	0x07, 0x1C, 0x30, 0x20, 0x00, 

	// @104 ')' (4 pixels wide)
	//     
	// #   
	//  #  
	//   # 
	//   # 
	//   ##
	//    #
	//    #
	//    #
	//    #
	//    #
	//   # 
	//   # 
	//  #  
	//     
	0x02, 0x04, 0x38, 0xE0, 
	0x00, 0x20, 0x18, 0x07, 

	// @112 '*' (7 pixels wide)
	//        
	//        
	//  #  #  
	//   # #  
	// #######
	// #####  
	//  #  #  
	//  #   # 
	//        
	//        
	//        
	//        
	//        
	//        
	//        
	0x30, 0xF4, 0x38, 0x30, 0x7C, 0x90, 0x10, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @126 '+' (6 pixels wide)
	//       
	//       
	//       
	//       
	//   #   
	//   #   
	//   #   
	// ######
	//   #   
	//   #   
	//   #   
	//       
	//       
	//       
	//       
	0x80, 0x80, 0xF0, 0x80, 0x80, 0x80, 
	0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 

	// @138 ',' (3 pixels wide)
	//    
	//    
	//    
	//    
	//    
	//    
	//    
	//    
	//    
	//   #
	//   #
	//   #
	// ## 
	//    
	//    
	0x00, 0x00, 0x00, 
	0x10, 0x10, 0x0E, 

	// @144 '-' (6 pixels wide)
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	// ######
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @156 '.' (2 pixels wide)
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	//   
	0x00, 0x00, 
	0x06, 0x06, 

	// @160 '/' (7 pixels wide)
	//        
	//       #
	//      ##
	//      # 
	//     #  
	//     #  
	//    #   
	//   #    
	//   #    
	//  #     
	// #      
	// #      
	//        
	//        
	//        
	0x00, 0x00, 0x80, 0x40, 0x30, 0x0C, 0x06, 
	0x0C, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 

	// @174 '0' (10 pixels wide)
	//           
	//           
	//     ####  
	//    #    # 
	//   #      #
	//  #       #
	// #        #
	// #       # 
	// #      #  
	// #    ##   
	//  ####     
	//           
	//           
	//           
	//           
	0xC0, 0x20, 0x10, 0x08, 0x04, 0x04, 0x04, 0x04, 0x88, 0x70, 
	0x03, 0x04, 0x04, 0x04, 0x04, 0x02, 0x02, 0x01, 0x00, 0x00, 

	// @194 '1' (8 pixels wide)
	//         
	//         
	//    ##   
	//   ###   
	// ##  #   
	//    #    
	//    #    
	//    #    
	//    #  ##
	//    ###  
	// ####    
	//         
	//         
	//         
	//         
	0x10, 0x10, 0x08, 0xEC, 0x1C, 0x00, 0x00, 0x00, 
	0x04, 0x04, 0x04, 0x07, 0x02, 0x02, 0x01, 0x01, 

	// @210 '2' (8 pixels wide)
	//         
	//         
	//    ###  
	//   #   # 
	//  #    # 
	//       # 
	//      #  
	//     #   
	//   ##    
	// ########
	// #       
	//         
	//         
	//         
	//         
	0x00, 0x10, 0x08, 0x04, 0x84, 0x44, 0x38, 0x00, 
	0x06, 0x02, 0x03, 0x03, 0x02, 0x02, 0x02, 0x02, 

	// @226 '3' (8 pixels wide)
	//         
	//         
	//         
	// ####### 
	//      #  
	//    ##   
	//   ##### 
	//        #
	//        #
	// #    ## 
	// #####   
	//         
	//         
	//         
	//         
	0x08, 0x08, 0x48, 0x68, 0x68, 0x58, 0x48, 0x80, 
	0x06, 0x04, 0x04, 0x04, 0x04, 0x02, 0x02, 0x01, 

	// @242 '4' (10 pixels wide)
	//           
	//           
	//        #  
	//      ###  
	//     ## #  
	//    ## #   
	//   ##  #   
	//  # #######
	// ###   #   
	//       #   
	//       #   
	//       #   
	//           
	//           
	//           
	0x00, 0x80, 0x40, 0xE0, 0xB0, 0x98, 0xE8, 0x9C, 0x80, 0x80, 
	0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 

	// @262 '5' (9 pixels wide)
	//          
	//          
	//          
	//    ######
	//   #      
	//  #       
	//  #####   
	//       #  
	//       #  
	// #    #   
	//  ####    
	//          
	//          
	//          
	//          
	0x00, 0x60, 0x50, 0x48, 0x48, 0x48, 0x88, 0x08, 0x08, 
	0x02, 0x04, 0x04, 0x04, 0x04, 0x02, 0x01, 0x00, 0x00, 

	// @280 '6' (8 pixels wide)
	//         
	//         
	//     ##  
	//    #    
	//   #     
	//  #      
	// #   ### 
	// #  #   #
	// # #    #
	// ###  ## 
	//  ####   
	//         
	//         
	//         
	//         
	0xC0, 0x20, 0x10, 0x88, 0x44, 0x44, 0x40, 0x80, 
	0x03, 0x06, 0x07, 0x04, 0x04, 0x02, 0x02, 0x01, 

	// @296 '7' (7 pixels wide)
	//        
	//        
	//        
	// #######
	//      # 
	//     #  
	//     #  
	//    #   
	//    #   
	//   #    
	//   #    
	//        
	//        
	//        
	//        
	0x08, 0x08, 0x08, 0x88, 0x68, 0x18, 0x08, 
	0x00, 0x00, 0x06, 0x01, 0x00, 0x00, 0x00, 

	// @310 '8' (7 pixels wide)
	//        
	//        
	//        
	//   #### 
	// ##   ##
	// #    # 
	//  ####  
	//   ###  
	//  #   # 
	// #    # 
	// #####  
	//        
	//        
	//        
	//        
	0x30, 0x50, 0xC8, 0xC8, 0xC8, 0x38, 0x10, 
	0x06, 0x05, 0x04, 0x04, 0x04, 0x03, 0x00, 

	// @324 '9' (7 pixels wide)
	//        
	//        
	//        
	//    ### 
	//  ##   #
	// #     #
	// #   ## 
	//  ### # 
	//     #  
	//     #  
	//     #  
	//        
	//        
	//        
	//        
	0x60, 0x90, 0x90, 0x88, 0x48, 0xC8, 0x30, 
	0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 

	// @338 ':' (2 pixels wide)
	//   
	//   
	//   
	//   
	//   
	// ##
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	//   
	0x20, 0x20, 
	0x06, 0x06, 

	// @342 ';' (3 pixels wide)
	//    
	//    
	//    
	//    
	//    
	//   #
	//    
	//    
	//    
	//   #
	//   #
	//  # 
	// #  
	//    
	//    
	0x00, 0x00, 0x20, 
	0x10, 0x08, 0x06, 

	// @348 '<' (5 pixels wide)
	//      
	//      
	//      
	//     #
	//   ## 
	//  ##  
	// ##   
	// ##   
	//   ## 
	//     #
	//      
	//      
	//      
	//      
	//      
	0xC0, 0xE0, 0x30, 0x10, 0x08, 
	0x00, 0x00, 0x01, 0x01, 0x02, 

	// @358 '=' (6 pixels wide)
	//       
	//       
	//       
	//       
	// ######
	//       
	//       
	// ######
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @370 '>' (6 pixels wide)
	//       
	//       
	//       
	// #     
	//  ##   
	//    #  
	//     ##
	//     # 
	//   ##  
	//  ##   
	//       
	//       
	//       
	//       
	//       
	0x08, 0x10, 0x10, 0x20, 0xC0, 0x40, 
	0x00, 0x02, 0x03, 0x01, 0x00, 0x00, 

	// @382 '?' (6 pixels wide)
	//       
	//   ### 
	//  #   #
	//      #
	//      #
	//    ## 
	//  ##   
	//  #    
	//  #    
	//       
	// ##    
	//       
	//       
	//       
	//       
	0x00, 0xC4, 0x42, 0x22, 0x22, 0x1C, 
	0x04, 0x05, 0x00, 0x00, 0x00, 0x00, 

	// @394 '@' (10 pixels wide)
	//           
	//      ###  
	//    ##  ## 
	//   ##    ##
	//  ##  ##  #
	//  #  # #  #
	// #  #  # ##
	// #  ###### 
	// #         
	//  ##     ##
	//   ######  
	//           
	//           
	//           
	//           
	0xC0, 0x30, 0x18, 0xCC, 0xA4, 0x92, 0xF2, 0x86, 0xCC, 0x78, 
	0x01, 0x02, 0x06, 0x04, 0x04, 0x04, 0x04, 0x04, 0x02, 0x02, 

	// @414 'A' (10 pixels wide)
	//           
	//           
	//      #    
	//     ##    
	//     # #   
	//    #  #   
	//    #  #   
	//   #   ##  
	//   #### #  
	//  #      # 
	// ##      # 
	// #        #
	//           
	//           
	//           
	0x00, 0x00, 0x80, 0x60, 0x18, 0x0C, 0xF0, 0x80, 0x00, 0x00, 
	0x0C, 0x06, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01, 0x06, 0x08, 

	// @434 'B' (8 pixels wide)
	//         
	//         
	//   ##### 
	// ##     #
	//  #     #
	//  #    # 
	//  # #### 
	// #      #
	// #      #
	// #      #
	// #    ## 
	// # ###   
	//         
	//         
	//         
	0x88, 0x78, 0x04, 0x44, 0x44, 0x44, 0x64, 0x98, 
	0x0F, 0x00, 0x08, 0x08, 0x08, 0x04, 0x04, 0x03, 

	// @450 'C' (8 pixels wide)
	//         
	//     ####
	//    ##  #
	//   ##   #
	//  ##     
	//  #      
	// #       
	// #       
	// #       
	// ##   ## 
	//  ####   
	//         
	//         
	//         
	//         
	0xC0, 0x30, 0x18, 0x0C, 0x06, 0x02, 0x02, 0x0E, 
	0x03, 0x06, 0x04, 0x04, 0x04, 0x02, 0x02, 0x00, 

	// @466 'D' (8 pixels wide)
	//         
	//         
	// ######  
	//  #    # 
	//  #     #
	//  #     #
	// #      #
	// #     ##
	// #    ## 
	// #  ###  
	// ####    
	// ##      
	//         
	//         
	//         
	0xC4, 0x3C, 0x04, 0x04, 0x04, 0x04, 0x88, 0xF0, 
	0x0F, 0x0C, 0x04, 0x06, 0x02, 0x03, 0x01, 0x00, 

	// @482 'E' (8 pixels wide)
	//         
	//         
	//    #####
	//  ##     
	//  #      
	//  #      
	//  ###### 
	//  #      
	// #       
	// #       
	// ####### 
	//         
	//         
	//         
	//         
	0x00, 0xF8, 0x48, 0x44, 0x44, 0x44, 0x44, 0x04, 
	0x07, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x00, 

	// @498 'F' (7 pixels wide)
	//        
	//        
	//  ######
	//  #     
	//  #     
	//  #     
	//  # ### 
	// ###    
	// #      
	// #      
	// #      
	// #      
	//        
	//        
	//        
	0x80, 0xFC, 0x84, 0x44, 0x44, 0x44, 0x04, 
	0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @512 'G' (8 pixels wide)
	//         
	//     ####
	//    ##  #
	//   #     
	//  #      
	//  #     #
	// #      #
	// #     # 
	// #    ## 
	// #   # # 
	//  ###  # 
	//       # 
	//         
	//         
	//         
	0xC0, 0x30, 0x08, 0x04, 0x06, 0x02, 0x82, 0x66, 
	0x03, 0x04, 0x04, 0x04, 0x02, 0x01, 0x0F, 0x00, 

	// @528 'H' (8 pixels wide)
	//         
	//  #     #
	//  #     #
	//  #     #
	//  #     #
	//  #    # 
	//  ###### 
	// #     # 
	// #     # 
	// #     # 
	// #     # 
	//         
	//         
	//         
	//         
	0x80, 0x7E, 0x40, 0x40, 0x40, 0x40, 0xE0, 0x1E, 
	0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 

	// @544 'I' (2 pixels wide)
	//   
	//   
	//  #
	// ##
	// ##
	// # 
	// # 
	// # 
	// # 
	// # 
	// # 
	//   
	//   
	//   
	//   
	0xF8, 0x1C, 
	0x07, 0x00, 

	// @548 'J' (7 pixels wide)
	//        
	//       #
	//       #
	//       #
	//       #
	//       #
	//       #
	//      # 
	// #    # 
	// ##  ## 
	//   ###  
	//        
	//        
	//        
	//        
	0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x7E, 
	0x03, 0x02, 0x04, 0x04, 0x06, 0x03, 0x00, 

	// @562 'K' (8 pixels wide)
	//         
	//         
	//  #    # 
	//  #   ## 
	//  #  ##  
	//  # #    
	//  ##     
	// ####    
	// #  ##   
	// #    #  
	// #     ##
	//         
	//         
	//         
	//         
	0x80, 0xFC, 0xC0, 0xA0, 0x10, 0x18, 0x0C, 0x00, 
	0x07, 0x00, 0x00, 0x01, 0x01, 0x02, 0x04, 0x04, 

	// @578 'L' (7 pixels wide)
	//        
	//        
	//  #     
	//  #     
	//  #     
	// ##     
	// #      
	// #      
	// #      
	// #      
	// #######
	//        
	//        
	//        
	//        
	0xE0, 0x3C, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x07, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 

	// @592 'M' (11 pixels wide)
	//            
	//            
	//   #       #
	//   ##     ##
	//   ##    # #
	//  # #   ### 
	//  # ## ## # 
	//  #  # #  # 
	//  #  ##   # 
	// ##       # 
	// #        # 
	// #          
	//            
	//            
	//            
	0x00, 0xE0, 0x1C, 0x78, 0xC0, 0x00, 0xC0, 0x60, 0x30, 0xE8, 0x1C, 
	0x0E, 0x03, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 

	// @614 'N' (9 pixels wide)
	//          
	//          
	//  #      #
	//  ##     #
	//  ##     #
	//  # #    #
	//  #  #  # 
	// ##  #  # 
	// #    # # 
	// #     ## 
	// #     ## 
	//          
	//          
	//          
	//          
	0x80, 0xFC, 0x18, 0x20, 0xC0, 0x00, 0x00, 0xC0, 0x3C, 
	0x07, 0x00, 0x00, 0x00, 0x00, 0x01, 0x06, 0x07, 0x00, 

	// @632 'O' (9 pixels wide)
	//          
	//          
	//    ####  
	//   ##   ##
	//  ##     #
	//  #      #
	// ##      #
	// #       #
	// #      # 
	// #     #  
	// ##   #   
	//  ####    
	//          
	//          
	//          
	0xC0, 0x70, 0x18, 0x0C, 0x04, 0x04, 0x04, 0x08, 0xF8, 
	0x07, 0x0C, 0x08, 0x08, 0x08, 0x04, 0x02, 0x01, 0x00, 

	// @650 'P' (8 pixels wide)
	//         
	//         
	//  ###### 
	// ##     #
	// ##     #
	// ##    # 
	// #   ##  
	// ####    
	// #       
	// #       
	// #       
	//         
	//         
	//         
	//         
	0xF8, 0xBC, 0x84, 0x84, 0x44, 0x44, 0x24, 0x18, 
	0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @666 'Q' (9 pixels wide)
	//          
	//      ##  
	//    ## ## 
	//   #    # 
	//  #      #
	//  #      #
	// #       #
	// #       #
	// #   #  # 
	// ##   ##  
	//  #### #  
	//       ## 
	//        # 
	//          
	//          
	0xC0, 0x30, 0x08, 0x04, 0x04, 0x02, 0x06, 0x0C, 0xF0, 
	0x03, 0x06, 0x04, 0x04, 0x05, 0x02, 0x0E, 0x19, 0x00, 

	// @684 'R' (8 pixels wide)
	//         
	//         
	//  #####  
	//  #    # 
	//  #    # 
	// #    #  
	// # ###   
	// #  ##   
	// #   ##  
	// #    ## 
	// #      #
	//         
	//         
	//         
	//         
	0xE0, 0x1C, 0x44, 0xC4, 0xC4, 0x24, 0x18, 0x00, 
	0x07, 0x00, 0x00, 0x00, 0x01, 0x03, 0x02, 0x04, 

	// @700 'S' (7 pixels wide)
	//        
	//        
	//   #####
	//  #    #
	// #      
	// #      
	//  ##    
	//   ###  
	//      # 
	//      # 
	//    ##  
	// ###    
	//        
	//        
	//        
	0x30, 0x48, 0xC4, 0x84, 0x84, 0x04, 0x0C, 
	0x08, 0x08, 0x08, 0x04, 0x04, 0x03, 0x00, 

	// @714 'T' (10 pixels wide)
	//           
	//       ####
	// ######    
	//     #     
	//     #     
	//     #     
	//     #     
	//     #     
	//     #     
	//     #     
	//     #     
	//           
	//           
	//           
	//           
	0x04, 0x04, 0x04, 0x04, 0xFC, 0x04, 0x02, 0x02, 0x02, 0x02, 
	0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @734 'U' (8 pixels wide)
	//         
	//         
	//  #     #
	//  #     #
	// #      #
	// #      #
	// #      #
	// #      #
	// #     ##
	//  #    # 
	//   ####  
	//         
	//         
	//         
	//         
	0xF0, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFC, 
	0x01, 0x02, 0x04, 0x04, 0x04, 0x04, 0x03, 0x01, 

	// @750 'V' (10 pixels wide)
	//           
	//          #
	// #       # 
	// ##     ## 
	//  #     #  
	//  #    #   
	//   #   #   
	//   #  #    
	//   ## #    
	//    ##     
	//    ##     
	//           
	//           
	//           
	//           
	0x0C, 0x38, 0xC0, 0x00, 0x00, 0x80, 0x60, 0x18, 0x0C, 0x02, 
	0x00, 0x00, 0x01, 0x07, 0x06, 0x01, 0x00, 0x00, 0x00, 0x00, 

	// @770 'W' (12 pixels wide)
	//             
	//             
	// #          #
	// #          #
	// ##    #    #
	//  #   ##   # 
	//  #   ##   # 
	//  #  #  #  # 
	//  # #   # #  
	//  ###   # #  
	//   #     #   
	//             
	//             
	//             
	//             
	0x1C, 0xF0, 0x00, 0x00, 0x80, 0x60, 0x70, 0x80, 0x00, 0x00, 0xE0, 0x1C, 
	0x00, 0x03, 0x06, 0x03, 0x00, 0x00, 0x00, 0x03, 0x04, 0x03, 0x00, 0x00, 

	// @794 'X' (9 pixels wide)
	//          
	//          
	//  #      #
	//  ##    # 
	//   #   #  
	//    # #   
	//     #    
	//    # #   
	//   #  ##  
	//  ##   ## 
	// ##     ##
	// #        
	//          
	//          
	//          
	0x00, 0x0C, 0x18, 0xA0, 0x40, 0xA0, 0x10, 0x08, 0x04, 
	0x0C, 0x06, 0x03, 0x00, 0x00, 0x01, 0x03, 0x06, 0x04, 

	// @812 'Y' (10 pixels wide)
	//           
	//           
	// ##       #
	//  ##     # 
	//   ##   #  
	//    #  #   
	//    ###    
	//     ##    
	//     #     
	//     #     
	//     #     
	//     #     
	//           
	//           
	//           
	0x04, 0x0C, 0x18, 0x70, 0xC0, 0xC0, 0x20, 0x10, 0x08, 0x04, 
	0x00, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @832 'Z' (9 pixels wide)
	//          
	//          
	//  ########
	//        # 
	//       ## 
	//      ##  
	//     ##   
	//    ##    
	//   ##     
	//  ##      
	// #########
	//          
	//          
	//          
	//          
	0x00, 0x04, 0x04, 0x84, 0xC4, 0x64, 0x34, 0x1C, 0x04, 
	0x04, 0x06, 0x07, 0x05, 0x04, 0x04, 0x04, 0x04, 0x04, 

	// @850 '[' (4 pixels wide)
	//     
	// ####
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// #   
	// ### 
	//     
	0xFE, 0x02, 0x02, 0x02, 
	0x3F, 0x20, 0x20, 0x00, 

	// @858 '\' (7 pixels wide)
	//        
	// #      
	//  #     
	//  ##    
	//   #    
	//   ##   
	//    #   
	//    ##  
	//     #  
	//     ## 
	//      # 
	//      ##
	//       #
	//        
	//        
	0x02, 0x0C, 0x38, 0xE0, 0x80, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x03, 0x0E, 0x18, 

	// @872 ']' (5 pixels wide)
	//      
	//  ####
	//     #
	//     #
	//    # 
	//    # 
	//    # 
	//    # 
	//    # 
	//    # 
	//    # 
	//    # 
	//    # 
	// #### 
	//      
	0x00, 0x02, 0x02, 0xF2, 0x0E, 
	0x20, 0x20, 0x20, 0x3F, 0x00, 

	// @882 '^' (7 pixels wide)
	//        
	//        
	//    #   
	//   ##   
	//   # #  
	//  #   # 
	// #     #
	//        
	//        
	//        
	//        
	//        
	//        
	//        
	//        
	0x40, 0x20, 0x18, 0x0C, 0x10, 0x20, 0x40, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 

	// @896 '_' (6 pixels wide)
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	// ######
	//       
	//       
	//       
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 

	// @908 '`' (4 pixels wide)
	//     
	// #   
	//  ## 
	//    #
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	//     
	0x02, 0x04, 0x04, 0x08, 
	0x00, 0x00, 0x00, 0x00, 

	// @916 'a' (7 pixels wide)
	//        
	//        
	//        
	//        
	//    ##  
	//   #  # 
	//  #   # 
	//  #  ## 
	// #   ## 
	// #  # # 
	// ###  ##
	//        
	//        
	//        
	//        
	0x00, 0xC0, 0x20, 0x10, 0x90, 0xE0, 0x00, 
	0x07, 0x04, 0x04, 0x02, 0x01, 0x07, 0x04, 

	// @930 'b' (6 pixels wide)
	//   #   
	//  ##   
	//  #    
	//  #    
	// ## ###
	// # ## #
	// ###  #
	// ##   #
	// #   # 
	// #  #  
	// ###   
	//       
	//       
	//       
	//       
	0xF0, 0xDE, 0x63, 0x30, 0x10, 0xF0, 
	0x07, 0x04, 0x04, 0x02, 0x01, 0x00, 

	// @942 'c' (5 pixels wide)
	//      
	//      
	//      
	//      
	//   ###
	//  ## #
	//  #   
	// #    
	// #    
	// #   #
	// #### 
	//      
	//      
	//      
	//      
	0x80, 0x60, 0x30, 0x10, 0x30, 
	0x07, 0x04, 0x04, 0x04, 0x02, 

	// @952 'd' (8 pixels wide)
	//         
	//        #
	//        #
	//       # 
	//       # 
	//    ## # 
	//  ##  #  
	//  #   #  
	// #   ##  
	// #  # #  
	// ###  ###
	//         
	//         
	//         
	//         
	0x00, 0xC0, 0x40, 0x20, 0x20, 0xC0, 0x38, 0x06, 
	0x07, 0x04, 0x04, 0x02, 0x01, 0x07, 0x04, 0x04, 

	// @968 'e' (6 pixels wide)
	//       
	//       
	//       
	//       
	//   ### 
	//  ## # 
	//  # #  
	// # #   
	// ##   #
	// #   # 
	//  ###  
	//       
	//       
	//       
	//       
	0x80, 0x60, 0xB0, 0x50, 0x30, 0x00, 
	0x03, 0x05, 0x04, 0x04, 0x02, 0x01, 

	// @980 'f' (5 pixels wide)
	//   ###
	//   # #
	//  #  #
	//  #   
	//  #   
	// #### 
	//  #   
	// #    
	// #    
	// #    
	// #    
	// #    
	//      
	//      
	//      
	0xA0, 0x7C, 0x23, 0x21, 0x07, 
	0x0F, 0x00, 0x00, 0x00, 0x00, 

	// @990 'g' (7 pixels wide)
	//        
	//        
	//        
	//        
	//     ###
	//    #  #
	//   #   #
	//   #  # 
	//  #   # 
	//  #  ## 
	//  ### # 
	//      # 
	//      # 
	// #   #  
	//  ####  
	0x00, 0x00, 0xC0, 0x20, 0x10, 0x90, 0x70, 
	0x20, 0x47, 0x44, 0x44, 0x62, 0x1F, 0x00, 

	// @1004 'h' (7 pixels wide)
	//   #    
	//   #    
	//  ##    
	//  ##    
	//  #     
	//  #  ## 
	//  # # # 
	// # #  # 
	// ##   # 
	// ##   ##
	// #      
	//        
	//        
	//        
	//        
	0x80, 0x7C, 0x8F, 0x40, 0x20, 0xE0, 0x00, 
	0x07, 0x03, 0x00, 0x00, 0x00, 0x03, 0x02, 

	// @1018 'i' (4 pixels wide)
	//     
	//   ##
	//   ##
	//     
	//     
	//  #  
	//  #  
	// #   
	// #   
	// #   
	// #   
	// #   
	//     
	//     
	//     
	0x80, 0x60, 0x06, 0x06, 
	0x0F, 0x00, 0x00, 0x00, 

	// @1026 'j' (6 pixels wide)
	//       
	//     ##
	//     ##
	//       
	//       
	//    ## 
	//    ## 
	//    #  
	//    #  
	//    #  
	//    #  
	//    #  
	//    #  
	// # ##  
	//  ##   
	0x00, 0x00, 0x00, 0xE0, 0x66, 0x06, 
	0x20, 0x40, 0x60, 0x3F, 0x00, 0x00, 

	// @1038 'k' (7 pixels wide)
	//   #    
	//  ##    
	//  ##    
	//  #     
	//  #   ##
	//  #  ## 
	//  # #   
	// # #    
	// ## #   
	// #   #  
	// #    ##
	//        
	//        
	//        
	//        
	0x80, 0x7E, 0x87, 0x40, 0x20, 0x30, 0x10, 
	0x07, 0x01, 0x00, 0x01, 0x02, 0x04, 0x04, 

	// @1052 'l' (2 pixels wide)
	//  #
	//  #
	//  #
	// # 
	// # 
	// # 
	// # 
	// # 
	// # 
	// # 
	// # 
	//   
	//   
	//   
	//   
	0xF8, 0x07, 
	0x07, 0x00, 

	// @1056 'm' (12 pixels wide)
	//             
	//             
	//             
	//             
	//  #       #  
	//  #  ##  # # 
	//  # # # #  # 
	// # #  # #  # 
	// ###  ##   # 
	// ##   ##   ##
	// #    #      
	//             
	//             
	//             
	//             
	0x80, 0x70, 0x80, 0x40, 0x20, 0xE0, 0x00, 0xC0, 0x20, 0x10, 0xE0, 0x00, 
	0x07, 0x03, 0x01, 0x00, 0x00, 0x07, 0x03, 0x00, 0x00, 0x00, 0x03, 0x02, 

	// @1080 'n' (8 pixels wide)
	//         
	//         
	//         
	//         
	//         
	// #       
	// #  ###  
	// # #  #  
	// # #  #  
	// ##   #  
	// ##    ##
	// #       
	//         
	//         
	//         
	0xE0, 0x00, 0x80, 0x40, 0x40, 0xC0, 0x00, 0x00, 
	0x0F, 0x06, 0x01, 0x00, 0x00, 0x03, 0x04, 0x04, 

	// @1096 'o' (5 pixels wide)
	//      
	//      
	//      
	//      
	//   ## 
	//  #  #
	// #   #
	// #   #
	// #   #
	// #  # 
	//  ##  
	//      
	//      
	//      
	//      
	0xC0, 0x20, 0x10, 0x10, 0xE0, 
	0x03, 0x04, 0x04, 0x02, 0x01, 

	// @1106 'p' (7 pixels wide)
	//        
	//        
	//        
	//        
	//  #  ###
	//  # #  #
	//  ##   #
	//  ##   #
	//  #   # 
	//  #  #  
	//  ###   
	// #      
	// #      
	// #      
	// #      
	0x00, 0xF0, 0xC0, 0x20, 0x10, 0x10, 0xF0, 
	0x78, 0x07, 0x04, 0x04, 0x02, 0x01, 0x00, 

	// @1120 'q' (5 pixels wide)
	//      
	//      
	//      
	//      
	//   ## 
	//  ## #
	//  #  #
	// #   #
	// #  ##
	// # ## 
	// ## # 
	//    # 
	//    # 
	//    # 
	//    # 
	0x80, 0x60, 0x30, 0x10, 0xE0, 
	0x07, 0x04, 0x02, 0x7F, 0x01, 

	// @1130 'r' (6 pixels wide)
	//       
	//       
	//       
	//       
	// ##  ##
	// #  # #
	// # #   
	// ##    
	// ##    
	// ##    
	// #     
	//       
	//       
	//       
	//       
	0xF0, 0x90, 0x40, 0x20, 0x10, 0x30, 
	0x07, 0x03, 0x00, 0x00, 0x00, 0x00, 

	// @1142 's' (5 pixels wide)
	//      
	//      
	//      
	//      
	//    ##
	//  ##  
	//  #   
	//  ##  
	//   ## 
	//     #
	//    ##
	// ###  
	//      
	//      
	//      
	0x00, 0xE0, 0xA0, 0x10, 0x10, 
	0x08, 0x08, 0x09, 0x05, 0x06, 

	// @1152 't' (6 pixels wide)
	//       
	//       
	//    #  
	//    #  
	//   ##  
	// ######
	//   #   
	//  #    
	//  #    
	//  #  # 
	//  ###  
	//       
	//       
	//       
	//       
	0x20, 0xA0, 0x70, 0x3C, 0x20, 0x20, 
	0x00, 0x07, 0x04, 0x04, 0x02, 0x00, 

	// @1164 'u' (7 pixels wide)
	//        
	//        
	//        
	//        
	//  #   # 
	//  #   # 
	// #   ## 
	// #   ## 
	// #  ### 
	// # ## # 
	// ###  ##
	//        
	//        
	//        
	//        
	0xC0, 0x30, 0x00, 0x00, 0xC0, 0xF0, 0x00, 
	0x07, 0x04, 0x06, 0x03, 0x01, 0x07, 0x04, 

	// @1178 'v' (8 pixels wide)
	//         
	//         
	//         
	//         
	// #      #
	// #     # 
	// #    #  
	//  #  #   
	//  # ##   
	//  ###    
	//   #     
	//         
	//         
	//         
	//         
	0x70, 0x80, 0x00, 0x00, 0x80, 0x40, 0x20, 0x10, 
	0x00, 0x03, 0x06, 0x03, 0x01, 0x00, 0x00, 0x00, 

	// @1194 'w' (10 pixels wide)
	//           
	//           
	//           
	//           
	//           
	// #        #
	// #   ##  # 
	// #  ##   # 
	// #  # #  # 
	//  ##  # #  
	//  ##   ##  
	//  #        
	//           
	//           
	//           
	0xE0, 0x00, 0x00, 0x80, 0xC0, 0x40, 0x00, 0x00, 0xC0, 0x20, 
	0x01, 0x0E, 0x06, 0x01, 0x00, 0x03, 0x04, 0x06, 0x01, 0x00, 

	// @1214 'x' (7 pixels wide)
	//        
	//        
	//        
	//        
	//  #    #
	//  #   # 
	//  #  #  
	//   ##   
	//  ###   
	//  # ##  
	// #   ## 
	// #      
	//        
	//        
	//        
	0x00, 0x70, 0x80, 0x80, 0x40, 0x20, 0x10, 
	0x0C, 0x03, 0x01, 0x03, 0x06, 0x04, 0x00, 

	// @1228 'y' (7 pixels wide)
	//        
	//        
	//        
	//        
	//        
	//   #   #
	//   #   #
	//  #    #
	//  #   ##
	//  #  # #
	//  ###  #
	//      # 
	//      # 
	// #    # 
	//  ####  
	0x00, 0x80, 0x60, 0x00, 0x00, 0x00, 0xE0, 
	0x20, 0x47, 0x44, 0x44, 0x42, 0x39, 0x07, 

	// @1242 'z' (7 pixels wide)
	//        
	//        
	//        
	//        
	//        
	//  ######
	//      # 
	//    ##  
	//   #    
	//  ######
	// ##     
	//        
	//        
	//        
	//        
	0x00, 0x20, 0x20, 0xA0, 0xA0, 0x60, 0x20, 
	0x04, 0x06, 0x03, 0x02, 0x02, 0x02, 0x02, 
};

// Character descriptors for Segoe Print 10pt
// { [Char width in bits], [Offset into segoePrint_10ptCharBitmaps in bytes] }
const FONT_CHAR_INFO segoePrint_10ptDescriptors[] = 
{
	{2, 0}, 		//   
	{3, 4}, 		// ! 
	{4, 10}, 		// " 
	{10, 18}, 		// # 
	{7, 38}, 		// $ 
	{8, 52}, 		// % 
	{11, 68}, 		// & 
	{2, 90}, 		// ' 
	{5, 94}, 		// ( 
	{4, 104}, 		// ) 
	{7, 112}, 		// * 
	{6, 126}, 		// + 
	{3, 138}, 		// , 
	{6, 144}, 		// - 
	{2, 156}, 		// . 
	{7, 160}, 		// / 
	{10, 174}, 		// 0 
	{8, 194}, 		// 1 
	{8, 210}, 		// 2 
	{8, 226}, 		// 3 
	{10, 242}, 		// 4 
	{9, 262}, 		// 5 
	{8, 280}, 		// 6 
	{7, 296}, 		// 7 
	{7, 310}, 		// 8 
	{7, 324}, 		// 9 
	{2, 338}, 		// : 
	{3, 342}, 		// ; 
	{5, 348}, 		// < 
	{6, 358}, 		// = 
	{6, 370}, 		// > 
	{6, 382}, 		// ? 
	{10, 394}, 		// @ 
	{10, 414}, 		// A 
	{8, 434}, 		// B 
	{8, 450}, 		// C 
	{8, 466}, 		// D 
	{8, 482}, 		// E 
	{7, 498}, 		// F 
	{8, 512}, 		// G 
	{8, 528}, 		// H 
	{2, 544}, 		// I 
	{7, 548}, 		// J 
	{8, 562}, 		// K 
	{7, 578}, 		// L 
	{11, 592}, 		// M 
	{9, 614}, 		// N 
	{9, 632}, 		// O 
	{8, 650}, 		// P 
	{9, 666}, 		// Q 
	{8, 684}, 		// R 
	{7, 700}, 		// S 
	{10, 714}, 		// T 
	{8, 734}, 		// U 
	{10, 750}, 		// V 
	{12, 770}, 		// W 
	{9, 794}, 		// X 
	{10, 812}, 		// Y 
	{9, 832}, 		// Z 
	{4, 850}, 		// [ 
	{7, 858}, 		// \ 
	{5, 872}, 		// ] 
	{7, 882}, 		// ^ 
	{6, 896}, 		// _ 
	{4, 908}, 		// ` 
	{7, 916}, 		// a 
	{6, 930}, 		// b 
	{5, 942}, 		// c 
	{8, 952}, 		// d 
	{6, 968}, 		// e 
	{5, 980}, 		// f 
	{7, 990}, 		// g 
	{7, 1004}, 		// h 
	{4, 1018}, 		// i 
	{6, 1026}, 		// j 
	{7, 1038}, 		// k 
	{2, 1052}, 		// l 
	{12, 1056}, 		// m 
	{8, 1080}, 		// n 
	{5, 1096}, 		// o 
	{7, 1106}, 		// p 
	{5, 1120}, 		// q 
	{6, 1130}, 		// r 
	{5, 1142}, 		// s 
	{6, 1152}, 		// t 
	{7, 1164}, 		// u 
	{8, 1178}, 		// v 
	{10, 1194}, 		// w 
	{7, 1214}, 		// x 
	{7, 1228}, 		// y 
	{7, 1242}, 		// z 
};

// Font information for Segoe Print 10pt
const FONT_INFO segoePrint_10ptFontInfo =
{
	2, //  Character height
	' ', //  Start character
	'z', //  End character
	2, //  Width, in pixels, of space character
	segoePrint_10ptDescriptors, //  Character descriptor array
	segoePrint_10ptBitmaps, //  Character bitmap array
};


