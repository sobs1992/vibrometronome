#include <stm32f0xx.h>
#include "i2c\i2c.h"
#include "LCD\lcd.h"
#include "SUPERTIMER.\supertimer.h"

#define EEPROM_ADDR     0xA0
#define VAR_N_bytes     6

#define TEMPO_MAX       360
#define BEAT_MAX        10
#define BEAT_POWER_MAX  10

#define PWR_ON          GPIOA -> BSRR = GPIO_BSRR_BS_1
#define PWR_OFF         GPIOA -> BSRR = GPIO_BSRR_BR_1

#define V1_ON           GPIOA -> BSRR = GPIO_BSRR_BS_5
#define V1_OFF          GPIOA -> BSRR = GPIO_BSRR_BR_5
#define V2_ON           GPIOA -> BSRR = GPIO_BSRR_BS_6
#define V2_OFF          GPIOA -> BSRR = GPIO_BSRR_BR_6

#define BUTTON_PWR      ((GPIOA -> IDR & GPIO_IDR_2) ? 0 : 1)
#define BUTTON_DEC      ((GPIOA -> IDR & GPIO_IDR_3) ? 1 : 0)
#define BUTTON_INC      ((GPIOA -> IDR & GPIO_IDR_4) ? 1 : 0)
#define BUTTON_SEL      ((GPIOB -> IDR & GPIO_IDR_1) ? 1 : 0)

uint8_t state, label_tempo, label_beat, beat_blink, vibro_blink, return_timer;
uint8_t on_off, current_beat, button[5], beat1_power_blink, beat0_power_blink;
uint16_t batt_level_adc;
uint8_t V_bat, vibro_timer, save_timer, save_flag, power_off, error_eeprom;

union _variables {
    uint8_t mas[VAR_N_bytes];
    #pragma pack(push, 1)
    struct {
        uint16_t tempo;
        uint8_t beats;
        uint8_t vibro_mode;
        uint8_t beat1_power;
        uint8_t beat0_power;
    };
    #pragma pack(pop)
};

union _variables var;

void GPIO_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;

    GPIOA -> OSPEEDR = 0xFFFFFFFF;

    GPIOA -> MODER |= GPIO_MODER_MODER1_0 | GPIO_MODER_MODER5_0 | GPIO_MODER_MODER6_0;
    GPIOA -> OTYPER |= GPIO_OTYPER_OT_5 | GPIO_OTYPER_OT_6;

    GPIOA -> PUPDR |= GPIO_PUPDR_PUPDR2_1 | GPIO_PUPDR_PUPDR3_0 | GPIO_PUPDR_PUPDR4_0;
    GPIOB -> PUPDR = GPIO_PUPDR_PUPDR1_0;

    V1_OFF;
    V2_OFF;
}

void SetPll48(){
    RCC -> CFGR |= RCC_CFGR_PLLMUL12;
    RCC -> CR |= RCC_CR_PLLON;
    while (!(RCC -> CR & RCC_CR_PLLRDY));
    RCC -> CFGR |= RCC_CFGR_SW_PLL;
}

uint8_t Button_GetState(uint8_t n){
    switch (n){
    case 1:
        return BUTTON_DEC;
        break;
    case 2:
        return BUTTON_SEL;
        break;
    case 3:
        return BUTTON_INC;
        break;
    default:
        return BUTTON_PWR;
        break;
    }
}

void Button_Timer(){
    static uint8_t button_prev[4] = {0}, pwr_cnt = 0, button_sel_flag[4] = {0};
    uint8_t i;
    for (i = 0; i < 4; i++){
        if ((Button_GetState(i) == 0) && (button_prev[i] == 0)){
            SuperTimer_Reset(return_timer);
            switch (i){
            case 0:
                if (pwr_cnt < 20) pwr_cnt++;
                else {
                    button[0] = 1;
                }
                break;
            case 1:
            case 3:
                if (state){
                    button_sel_flag[i] = 1;
                } else {
                    button[i] = 1;
                }
                break;
            case 2:
                button_sel_flag[i] = 1;
                break;
            default:
                break;
            }
        } else {
            switch (i){
            case 0:
                if ((pwr_cnt > 0) && (pwr_cnt < 10)){
                    button[4] = 1;
                }
                pwr_cnt = 0;
                break;
            case 1:
            case 2:
            case 3:
                if (button_sel_flag[i]){
                    button_sel_flag[i] = 0;
                    button[i] = 1;
                }
                break;
            default:
                break;
            }
        }
        button_prev[i] = Button_GetState(i);
    }
}

void Blink_Timer(){
    switch (state){
    case 1:
        beat_blink = !beat_blink;
        break;
    case 2:
        vibro_blink = !vibro_blink;
        break;
    case 3:
        beat1_power_blink = !beat1_power_blink;
        break;
    case 4:
        beat0_power_blink = !beat0_power_blink;
        break;
    default:
        break;
    }
}

uint8_t Restore_Settings(){
    uint8_t stat = I2C_ReadBuf(EEPROM_ADDR, 0, var.mas, VAR_N_bytes);
    if ((var.tempo > TEMPO_MAX) || (stat)) var.tempo = 120;
    if ((var.beats < 2) || (var.beats > BEAT_MAX) || (stat)) var.beats = 4;
    if ((var.vibro_mode > 2) || (stat)) var.vibro_mode = 0;
    if ((var.beat1_power < 1) || (var.beat1_power > BEAT_POWER_MAX) || (stat)) var.beat1_power = 2;
    if ((var.beat1_power < 1) || (var.beat0_power > BEAT_POWER_MAX) || (stat)) var.beat0_power = 2;
    return stat;
}

void Save_Settings(){
    I2C_WriteBuf(EEPROM_ADDR, 0, VAR_N_bytes, var.mas);
}

void Return_Timer(){
    state = 0xFF;
    beat_blink = 0;
    vibro_blink = 0;
    beat0_power_blink = 0;
    beat1_power_blink = 0;
}

void ADC_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;
    RCC -> APB2ENR |= RCC_APB2ENR_ADC1EN;

    GPIOA -> MODER |= GPIO_MODER_MODER0;

    ADC1 -> IER = ADC_IER_EOCIE;
    ADC1 -> CFGR1 = ADC_CFGR1_CONT;
    ADC1 -> CFGR2 = ADC_CFGR2_JITOFFDIV4;
    ADC1 -> SMPR = ADC_SMPR1_SMPR;
    ADC1 -> CHSELR = ADC_CHSELR_CHSEL0;
    ADC1 -> CR &= ~ADC_CR_ADEN;
    ADC1 -> CR = ADC_CR_ADCAL;
    while(ADC1 -> CR & ADC_CR_ADCAL);
    ADC1 -> CR |= ADC_CR_ADEN;
    while(!(ADC1 -> ISR & ADC_ISR_ADRDY));
    ADC1 -> CR |= ADC_CR_ADSTART;

    NVIC_SetPriority(ADC1_IRQn, 2);
    NVIC_EnableIRQ(ADC1_IRQn);
}

/*
    0%    - 3,5Vacc = 2,25Vadc = 3127
    100%  - 4,2Vacc = 3,02Vadc = 3747
    3747 - 3127 = 620
*/
void ADC_IRQHandler(){
    uint16_t temp;
    if (ADC1 -> ISR & ADC_ISR_EOC){
        temp = ADC1 -> DR;
        if (temp < 3127){
            batt_level_adc = 0;
            power_off = 1;
            return;
        }  
        temp -= 3127;
        batt_level_adc += (temp - batt_level_adc) / 2;
        V_bat = batt_level_adc / 62; //(10 * ADC) / (3747 - 3127)
    }
}

void Vibro_Timer(){
    V1_OFF;
    V2_OFF;
}

void Metronome_Init(){
    RCC -> APB1ENR |= RCC_APB1ENR_TIM14EN;
    
    vibro_timer = SuperTimer_Create(T_MODE_OPM, 100, Vibro_Timer);

    TIM14 -> PSC = 0x00;
    TIM14 -> ARR = 10000;
    TIM14 -> EGR = TIM_EGR_UG;
    TIM14 -> SR;
    TIM14 -> SR = ~TIM_SR_UIF;
    TIM14 -> DIER = TIM_DIER_UIE;
    NVIC_SetPriority(TIM14_IRQn, 0);
    NVIC_EnableIRQ(TIM14_IRQn);
}

void Metronome_ChangePeriod(uint16_t tempo){
    TIM14 -> PSC = ((4800UL * 60UL) / (tempo)) - 1;
}

void TIM14_IRQHandler(){
    TIM14 -> SR = ~TIM_SR_UIF;
    if (on_off == 0) return;

    if (current_beat < var.beats - 1) current_beat++;
    else current_beat = 0;

    switch (var.vibro_mode){
    case 0:
        V1_ON;
        V2_ON;
        break;
    case 1:
        V1_ON;
        if (current_beat == 0) V2_ON;
        break;
    case 2:
        V1_ON;
        break;
    default:
        var.vibro_mode = 0;
        break;
    }

    if (current_beat == 0) SuperTimer_ChangePeriod(vibro_timer, 10 + var.beat1_power * 10);
    else SuperTimer_ChangePeriod(vibro_timer, 10 + var.beat0_power * 10);

    SuperTimer_En(vibro_timer, T_ON);
}

void Save_Timer(){
    save_flag = 1;
}

void main(void) {
    uint8_t i, button_timer, blink_timer;

    GPIO_Init();
    SetPll48();
    SuperTimer_Init();
    
    delay_ms(500);

    I2C_Init();

    LCD_Init();

    LCD_Fill(0, 0, 127, 7, 0x00);
    LCD_Text(40, 0, segoePrint_10ptFontInfo, "HELLO!");
    LCD_Text(0, 3, segoePrint_10ptFontInfo, "vibrometronome");
    LCD_Text(40, 5, segoePrint_10ptFontInfo, "by sob");
    
    PWR_ON;
    for (i = 0; i < 7; i++){
        V1_ON;
        V2_ON;
        delay_ms(50);
        V1_OFF;
        V2_OFF;
        delay_ms(50);
    }
    delay_ms(500);

    error_eeprom = Restore_Settings();

    if (error_eeprom){
        LCD_Fill(0, 0, 127, 7, 0x00);
        LCD_Text(45, 1, segoePrint_10ptFontInfo, "ERROR");
        LCD_Text(38, 4, segoePrint_10ptFontInfo, "EEPROM");
        delay_ms(1500);
    }
    
    LCD_Fill(0, 0, 127, 7, 0x00);

    LCD_Text(5, 2, segoePrint_10ptFontInfo, "BEAT");
    LCD_Text(60, 2, segoePrint_10ptFontInfo, "TEMPO");
    label_tempo = LCD_Create_Label(60, 4, segoePrint_20ptFontInfo, " ");
    label_beat = LCD_Create_Label(15, 4, segoePrint_20ptFontInfo, " ");

    button_timer = SuperTimer_Create(T_MODE_NORMAL, 50, Button_Timer);
    SuperTimer_En(button_timer, T_ON);
    blink_timer = SuperTimer_Create(T_MODE_NORMAL, 200, Blink_Timer);
    return_timer = SuperTimer_Create(T_MODE_OPM, 5000, Return_Timer);
    save_timer = SuperTimer_Create(T_MODE_OPM, 5000, Save_Timer);

    ADC_Init();
    current_beat = 0;
    on_off = 0;
    Metronome_Init();
    Metronome_ChangePeriod(var.tempo);

    IWDG -> KR = 0x0000CCCC;
    IWDG -> KR = 0x00005555;
    IWDG -> PR = 7;
    IWDG -> RLR = 2047;
    while (IWDG -> SR);
    IWDG -> KR = 0x0000AAAA;

    while (1){
        IWDG -> KR = 0x0000AAAA;
        LCD_Draw_Battery(110, 0, V_bat);
        if (on_off) LCD_Bitmap(102, 0, 4, 1, play);
        else LCD_Bitmap(102, 0, 4, 1, stop);

        for (i = 0; i < 10; i++){
            if (i < var.beats){
                if ((i == (var.beats - 1)) && (beat_blink)){
                    LCD_Fill(i * 10, 0, i * 10 + 8, 1, 0x00);
                } else if (i == current_beat) LCD_Bitmap(i * 10, 0, 8, 1, beat_1);
                else LCD_Bitmap(i * 10, 0, 8, 1, beat_0);
            } else LCD_Fill(i * 10, 0, i * 10 + 8, 1, 0x00);

            if (i < var.beat1_power){
                if ((i == (var.beat1_power - 1)) && (beat1_power_blink)){
                    LCD_Fill(60 + 3 * i, 7, 62 + 3 * i, 8, 0x00);
                } else LCD_Bitmap(60 + 3 * i, 7, 3, 1, time_power_icon);
            } else {
                LCD_Fill(60 + 3 * i, 7, 62 + 3 * i, 8, 0x00);
            }

            if (i < var.beat0_power){
                if ((i == (var.beat0_power - 1)) && (beat0_power_blink)){
                    LCD_Fill(95 + 3 * i, 7, 97 + 3 * i, 8, 0x00);
                } else LCD_Bitmap(95 + 3 * i, 7, 3, 1, time_icon);
            } else {
                LCD_Fill(95 + 3 * i, 7, 97 + 3 * i, 8, 0x00);
            }
        }
        
        if (vibro_blink == 0) {
            switch (var.vibro_mode){
            case 0:
                LCD_Bitmap(0, 7, 13, 1, vibro_icon);
                LCD_Bitmap(15, 7, 5, 1, plus_icon);
                LCD_Bitmap(22, 7, 13, 1, vibro_icon);
                break;
            case 1:
                LCD_Bitmap(0, 7, 13, 1, vibro_power_icon);
                LCD_Bitmap(15, 7, 5, 1, plus_icon);
                LCD_Bitmap(22, 7, 13, 1, vibro_icon);
                break;
            case 2:
                LCD_Bitmap(0, 7, 13, 1, vibro_icon);
                LCD_Fill(13, 7, 35, 7, 0x00);
                break;
            default:
                var.vibro_mode = 0;
                break;
            }
        } else {
            LCD_Fill(0, 7, 35, 7, 0x00);
        }

        if (button[0]){
            button[0] = 0;
            power_off = 1;
        }

        if (button[4]){
            button[4] = 0;
            on_off = !on_off;
        }

        if (button[2]){
            button[2] = 0;
            state++;
            switch (state){
            case 1:
            case 2:
            case 3:
            case 4:
                SuperTimer_En(blink_timer, T_ON);
                SuperTimer_En(return_timer, T_ON);
                break;
            default:
                SuperTimer_En(blink_timer, T_OFF);
                break;
            }
            beat_blink = 0;
            vibro_blink = 0;
            beat0_power_blink = 0;
            beat1_power_blink = 0;
        }

        switch (state){
        case 0:
            if (button[1]){
                button[1] = 0;
                if (var.tempo){
                    var.tempo--;
                    Metronome_ChangePeriod(var.tempo);
                    SuperTimer_En(save_timer, T_ON);
                }
            }
            if (button[3]){
                button[3] = 0;
                if (var.tempo < TEMPO_MAX){ 
                    var.tempo++;
                    Metronome_ChangePeriod(var.tempo);
                    SuperTimer_En(save_timer, T_ON);
                }
            }
            break;
        case 1:
            if (button[1]){
                button[1] = 0;
                if (var.beats > 2) var.beats--;
            }
            if (button[3]){
                button[3] = 0;
                if (var.beats < BEAT_MAX) var.beats++;
            }
            break;
        case 2:
            if ((button[1]) || (button[3])){
                button[1] = 0;
                button[3] = 0;
                var.vibro_mode++;
            }
            break;
        case 3:
            if (button[1]){
                button[1] = 0;
                if (var.beat1_power > 1) var.beat1_power--;
            }
            if (button[3]){
                button[3] = 0;
                if (var.beat1_power < BEAT_POWER_MAX) var.beat1_power++;
            }
            break;
        case 4:
            if (button[1]){
                button[1] = 0;
                if (var.beat0_power > 1) var.beat0_power--;
            }
            if (button[3]){
                button[3] = 0;
                if (var.beat0_power < BEAT_POWER_MAX) var.beat0_power++;
            }
            break;
        default:
            save_flag = 1;
            state = 0;
            break;
        }

        if (save_flag){
            save_flag = 0;
            if (!error_eeprom) Save_Settings();
        }

        if (power_off){
            LCD_Fill(0, 0, 127, 7, 0x00);
            LCD_Text(20, 1, segoePrint_10ptFontInfo, "SHUTDOWN");
            LCD_Text(20, 4, segoePrint_10ptFontInfo, "GOOD BYE!!!");
            delay_ms(1000);
            PWR_OFF;
            delay_ms(1000);
        }

        LCD_Label_Num(label_tempo, var.tempo);
        LCD_Label_Num(label_beat, current_beat + 1);

        if (on_off) TIM14 -> CR1 = TIM_CR1_CEN;
        else TIM14 -> CR1 = 0;
    }
}