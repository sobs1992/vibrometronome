#include "supertimer.h"

#define MAX_TIMERS  10

typedef struct {
    unsigned id : 4;	    //max 16 timers
    unsigned mode : 2;
    unsigned en : 1;
    GPIO_TypeDef* gpio;
    uint32_t duty;
    uint8_t pin;
    uint32_t period;
    uint32_t counter;
    void (*pFunc)();
} _timer;

static _timer timers[MAX_TIMERS];
static uint32_t delay_counter;

uint8_t SuperTimer_Create(uint8_t mode, uint32_t period, void (*pFunc)()){
    uint8_t i;
    for (i = 0; i < MAX_TIMERS; i++){
	if (timers[i].id == 0){
	    timers[i].id = i + 1;
	    timers[i].mode = mode;
	    timers[i].en = 0;
	    timers[i].period = period;
            timers[i].counter = period;
	    timers[i].pFunc = pFunc;
	    break;
	}
    }
    if (i >= MAX_TIMERS) return 0xFF;
    return i + 1;
}

void SuperTimer_ChangePeriod(uint8_t id, uint32_t period){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].counter = period;
    timers[id - 1].period = period;
}

void SuperTimer_PWMChangeDuty(uint8_t id, uint32_t duty){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].duty = duty;
}

uint32_t SuperTimer_PWMGetDuty(uint8_t id){
    if (id == 0) return 0;
    if (id > MAX_TIMERS) return 0;
    return timers[id - 1].duty;
}

void SuperTimer_En(uint8_t id, uint8_t state){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].en = state;
}

void SuperTimer_Reset(uint8_t id){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].counter = timers[id - 1].period;
}

uint8_t SuperTimer_CreatePWM(GPIO_TypeDef* gpio, uint8_t pin, uint32_t period, uint32_t duty){
    uint8_t i;
    for (i = 0; i < MAX_TIMERS; i++){
	if (timers[i].id == 0){
	    timers[i].id = i + 1;
	    timers[i].mode = T_MODE_PWM;
	    timers[i].en = 0;
	    timers[i].period = period;
            timers[i].counter = period;
	    timers[i].gpio = gpio;
	    timers[i].pin = pin;
	    timers[i].duty = duty;
	    break;
	}
    }
    if (i >= MAX_TIMERS) return 0xFF;
    return i + 1;
}

void SysTick_Handler(){
    uint8_t i;
    if (delay_counter > 0) delay_counter--;
    for (i = 0; i < MAX_TIMERS; i++){
	if (timers[i].id == 0) break;
	if (timers[i].en == T_ON){
	    if (timers[i].counter > 0){
		timers[i].counter--;
	    } else {
		if (timers[i].mode == T_MODE_OPM){
		    timers[i].en = T_OFF;
		}
		timers[i].counter = timers[i].period;
		if (timers[i].mode != T_MODE_PWM){
		    timers[i].pFunc();
		}
	    }
	    if (timers[i].mode == T_MODE_PWM){
		if ((timers[i].period - timers[i].counter) < timers[i].duty){
		    timers[i].gpio -> BSRR = 1 << timers[i].pin; 
		} else {
		    timers[i].gpio -> BSRR = (1 << timers[i].pin) << 16; 
		}
	    }
	}
    }
}

void delay_ms(uint16_t delay){
    delay_counter = delay;
    while (delay_counter);
}

void SuperTimer_Init(){
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);
    NVIC_SetPriority (SysTick_IRQn, 5);
    delay_counter = 0;
}