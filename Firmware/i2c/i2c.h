#pragma once
#include <stm32f0xx.h>

void I2C_Init();
void I2C_Write(uint8_t id, uint8_t addr, uint8_t byte);
void I2C_WriteBuf(uint8_t id, uint8_t addr, uint16_t length, uint8_t *buf);
void I2C_Read(uint8_t id, uint8_t addr, uint8_t *byte);
uint8_t I2C_ReadBuf(uint8_t id, uint8_t addr, uint8_t *buf, uint8_t length);
void I2C_WriteBytes(uint8_t id, uint8_t addr, uint16_t length, uint8_t byte);