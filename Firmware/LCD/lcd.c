#include "lcd.h"

const uint8_t play[] = {
    0b01111111,
    0b00111110,
    0b00011100,
    0b00001000
};

const uint8_t stop[] = {
    0b01111111,
    0b00000000,
    0b00000000,
    0b01111111
};

const uint8_t beat_0[] = {
    0b11111111,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b10000001,
    0b11111111
};

const uint8_t beat_1[] = {
    0b11111111,
    0b10000001,
    0b10111101,
    0b10111101,
    0b10111101,
    0b10111101,
    0b10000001,
    0b11111111
};

const uint8_t plus_icon[] = {
    0b00010000,
    0b00010000,
    0b01111100,
    0b00010000,
    0b00010000
};

const uint8_t vibro_icon[] = {
    0b00111000,
    0b00000000,
    0b01111100,
    0b00000000,
    0b11111111,
    0b10000001,
    0b10000001,
    0b10000001,
    0b11111111,
    0b00000000,
    0b01111110,
    0b00000000,
    0b00111000
};

const uint8_t vibro_power_icon[] = {
    0b00111000,
    0b00000000,
    0b01111100,
    0b00000000,
    0b01111110,
    0b11111111,
    0b11111111,
    0b11111111,
    0b01111110,
    0b00000000,
    0b01111110,
    0b00000000,
    0b00111000
};

const uint8_t time_power_icon[] = {
    0b11111111,
    0b11111111,
    0b00000000
};

const uint8_t time_icon[] = {
    0b00000000,
    0b11111111,
    0b00000000
};

_label label[LABEL_N];

void LCD_Init(){
    I2C_Write(LCD_ID, 0x80, 0xA8);
    I2C_Write(LCD_ID, 0x80, 0x3F);
    I2C_Write(LCD_ID, 0x80, 0xD3);
    I2C_Write(LCD_ID, 0x80, 0x00);

    I2C_Write(LCD_ID, 0x80, 0x40);
    I2C_Write(LCD_ID, 0x80, 0xA1);
    I2C_Write(LCD_ID, 0x80, 0xC8);

    I2C_Write(LCD_ID, 0x80, 0xDA);
    I2C_Write(LCD_ID, 0x80, 0x12);
    I2C_Write(LCD_ID, 0x80, 0x81);
    I2C_Write(LCD_ID, 0x80, 0x80); //bright

    I2C_Write(LCD_ID, 0x80, 0xA4);
    I2C_Write(LCD_ID, 0x80, 0xA6);
    I2C_Write(LCD_ID, 0x80, 0xD5);
    I2C_Write(LCD_ID, 0x80, 0x80);
    I2C_Write(LCD_ID, 0x80, 0x8D);
    I2C_Write(LCD_ID, 0x80, 0x14);
    I2C_Write(LCD_ID, 0x80, 0xAF);

    I2C_Write(LCD_ID, 0x80, 0x20);
    I2C_Write(LCD_ID, 0x80, 0x00);
}

void LCD_SetWindow(uint8_t sx, uint8_t sy, uint8_t ex, uint8_t ey){
    if ((sx > 127) || (ex > 127) || (sy > 7) || (sy > 7)) return;

    I2C_Write(LCD_ID, 0x80, 0x21);
    I2C_Write(LCD_ID, 0x80, sx);
    I2C_Write(LCD_ID, 0x80, ex);

    I2C_Write(LCD_ID, 0x80, 0x22);
    I2C_Write(LCD_ID, 0x80, sy);
    I2C_Write(LCD_ID, 0x80, ey);
}

void LCD_Fill(uint8_t sx, uint8_t sy, uint8_t ex, uint8_t ey, uint8_t color){
    LCD_SetWindow(sx, sy, ex, ey);
    I2C_WriteBytes(LCD_ID, 0x40, (ex - sx + 1) * (ey - sy + 1), (color) ? 0xFF : 0x00);
}

uint8_t LCD_Char(uint8_t x, uint8_t y, FONT_INFO font, uint8_t sym){
    if (sym < 'a') sym -= 32;
    else sym -= 33;

    LCD_SetWindow(x, y, x + font.FontTable[sym].widthBits - 1, y + font.Height);
    I2C_WriteBuf(LCD_ID, 0x40, font.FontTable[sym].widthBits * font.Height, 
                  &font.FontBitmaps[font.FontTable[sym].start]);
    return x + font.FontTable[sym].widthBits;
}

uint8_t LCD_Text(uint8_t x, uint8_t y, FONT_INFO font, uint8_t *text){
    uint8_t pos = x;
    while (*text){
        pos = LCD_Char(pos, y, font, *text);
        pos = LCD_Char(pos, y, font, ' ');
        text++;
    }
    return pos;
}

uint8_t GetTextLen(FONT_INFO font, uint8_t *text){
    uint8_t len = 0, sym;
    while (*text){
        sym = *text;
        if (sym < 'a') sym -= 32;
        else sym -= 33;
        len += font.FontTable[sym].widthBits + font.FontTable[0].widthBits;
        text++;
    }
    return len;
}

void LCD_Bitmap(uint8_t x, uint8_t y, uint8_t sx, uint8_t sy, const uint8_t *bitmap){
    LCD_SetWindow(x, y, x + sx - 1, y + sy);
    I2C_WriteBuf(LCD_ID, 0x40, sx * sy, bitmap);
}

void LCD_BitmapCol(uint8_t x, uint8_t y, uint8_t bitmap_col){
    LCD_SetWindow(x, y, x, y);
    I2C_Write(LCD_ID, 0x40, bitmap_col);
}

void LCD_Draw_Battery(uint8_t x, uint8_t y, uint8_t level){
    uint8_t i;
    if (level > 10) level = 10;
    LCD_SetWindow(x, y, x + 14, y);
    I2C_Write(LCD_ID, 0x40, 0xFF);
    I2C_Write(LCD_ID, 0x40, 0x81);
    for (i = 0; i < 10; i++){
        if (i < level) I2C_Write(LCD_ID, 0x40, 0xBD);
        else I2C_Write(LCD_ID, 0x40, 0x81);
    }
    I2C_Write(LCD_ID, 0x40, 0x81);
    I2C_Write(LCD_ID, 0x40, 0x7E);
    I2C_Write(LCD_ID, 0x40, 0x18);
}

uint8_t LCD_Create_Label(uint8_t x, uint8_t y, FONT_INFO font, uint8_t *text){
    uint8_t i;
    for (i = 0; i < LABEL_N; i++){
        if (label[i].text_width == 0) break;
    }
    label[i].x = x;
    label[i].y = y;
    label[i].font = font;
    label[i].text_width = GetTextLen(font, text);
    LCD_Text(x, y, font, text);
    return i;
}

void LCD_Label_Text(uint8_t id, uint8_t *text){
    uint8_t len = GetTextLen(label[id].font, text);
    uint8_t pos, i;
    pos = LCD_Text(label[id].x, label[id].y, label[id].font, text);
    if (len < label[id].text_width){
        len = label[id].text_width - len;
        LCD_SetWindow(pos, label[id].y, pos + len - 1, label[id].y + label[id].font.Height);
        for (i = 0; i < (len * label[id].font.Height); i++){
            I2C_Write(LCD_ID, 0x40, 0x00);
        }
    }
    label[id].text_width = len;
}

void LCD_Label_Num(uint8_t id, uint16_t num){
    uint8_t temp[4];
    if (num > 999) return;
    itoa(num, temp, 10);
    LCD_Label_Text(id, temp);
}
