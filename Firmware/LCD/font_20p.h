#include <stdint.h>
#include "fontStructures.h"

/* Font data for Segoe Print 20pt */
extern const uint8_t segoePrint_20ptBitmaps[];
extern const FONT_INFO segoePrint_20ptFontInfo;
extern const FONT_CHAR_INFO segoePrint_20ptDescriptors[];