#ifndef SUPERTIMER_H
#define SUPERTIMER_H

#include <stm32f0xx.h>

#define T_MODE_NORMAL	0
#define T_MODE_OPM	1
#define T_MODE_PWM	2
#define T_ON		1
#define T_OFF		0

void delay_ms(uint16_t delay);
void SuperTimer_Init();
void SuperTimer_En(uint8_t id, uint8_t state);
void SuperTimer_Reset(uint8_t id);
void SuperTimer_ChangePeriod(uint8_t id, uint32_t period);
uint8_t SuperTimer_Create(uint8_t mode, uint32_t period, void (*pFunc)());
uint8_t SuperTimer_CreatePWM(GPIO_TypeDef* gpio, uint8_t pin, uint32_t period, uint32_t duty);
void SuperTimer_PWMChangeDuty(uint8_t id, uint32_t duty);
uint32_t SuperTimer_PWMGetDuty(uint8_t id);

#endif