#include "i2c.h"

uint8_t I2C_CheckState(){
    while (((I2C1 -> ISR & I2C_ISR_TXIS) == 0) && ((I2C1 -> ISR & I2C_ISR_NACKF) == 0) && 
    ((I2C1 -> ISR & I2C_ISR_TCR) == 0) && ((I2C1 -> ISR & I2C_ISR_STOPF) == 0));
    if (I2C1 -> ISR & I2C_ISR_NACKF){
	I2C1 -> ICR = I2C_ICR_NACKCF;
	return 1;
    }
    if (I2C1 -> ISR & I2C_ISR_STOPF){
	I2C1 -> ICR = I2C_ICR_STOPCF;
	//return 1;
    }
    return 0;
}

void I2C_Write(uint8_t id, uint8_t addr, uint8_t byte){
    I2C1 -> CR2 = I2C_CR2_AUTOEND | (((uint32_t)(2) << 16)) | id; 
    I2C1 -> CR2 |= I2C_CR2_START;
    if (I2C_CheckState() == 1) return;
    I2C1 -> TXDR = addr;
    if (I2C_CheckState() == 1) return;
    I2C1 -> TXDR = byte;
    while (I2C1 -> ISR & I2C_ISR_BUSY);
}

void I2C_Read(uint8_t id, uint8_t addr, uint8_t *byte){
    I2C1 -> CR2 = (((uint32_t)(1) << 16)) | id; 
    I2C1 -> CR2 |= I2C_CR2_START;
    if (I2C_CheckState() == 1) return;
    I2C1 -> TXDR = addr;
    while (!(I2C1 -> ISR & I2C_ISR_TC));
    I2C1 -> CR2 = I2C_CR2_AUTOEND | I2C_CR2_RD_WRN | (((uint32_t)(1) << 16)) | id | I2C_CR2_START; 
    while (!(I2C1 -> ISR & I2C_ISR_RXNE));
    *byte = I2C1 -> RXDR; 
    while (I2C1 -> ISR & I2C_ISR_BUSY);
}

uint8_t I2C_ReadBuf(uint8_t id, uint8_t addr, uint8_t *buf, uint8_t length){
    uint16_t i;
    I2C1 -> CR2 = (((uint32_t)(1) << 16)) | id; 
    I2C1 -> CR2 |= I2C_CR2_START;
    if (I2C_CheckState() == 1) return 1;
    I2C1 -> TXDR = addr;
    i = 0;
    while (!(I2C1 -> ISR & I2C_ISR_TC)){
        if (i < 10000) i++;
        else return  1;
    }
    I2C1 -> CR2 = I2C_CR2_AUTOEND | I2C_CR2_RD_WRN | (((uint32_t)(length) << 16)) | id | I2C_CR2_START;
    while (length){
	length--;
        i = 0;
	while (!(I2C1 -> ISR & I2C_ISR_RXNE)){
            if (i < 10000) i++;
            else return  1;
        }
	*buf++ = I2C1 -> RXDR; 
    }
    i = 0;
    while (I2C1 -> ISR & I2C_ISR_BUSY){
        if (i < 10000) i++;
        else return  1;
    }
    return  0;
}

void I2C_WriteBuf(uint8_t id, uint8_t addr, uint16_t length, uint8_t *buf){
    uint8_t i;
    I2C1 -> CR2 = I2C_CR2_AUTOEND | (((uint32_t)(length + 1) << 16)) | id; 
    I2C1 -> CR2 |= I2C_CR2_START;
    if (I2C_CheckState() == 1) return;
    I2C1 -> TXDR = addr;
    if (I2C_CheckState() == 1) return;
    while (length){
	length--;
	I2C1 -> TXDR = *buf++;
	if (I2C_CheckState() == 1) return;
    }
    while (I2C1 -> ISR & I2C_ISR_BUSY){
        if (i < 10000) i++;
        else return  1;
    }
}

void I2C_WriteBytes(uint8_t id, uint8_t addr, uint16_t length, uint8_t byte){
    uint8_t i;
    if (length > 255) I2C1 -> CR2 = I2C_CR2_AUTOEND | I2C_CR2_RELOAD | (((uint32_t)(255) << 16)) | id; 
    else I2C1 -> CR2 = I2C_CR2_AUTOEND | (((uint32_t)(length + 1) << 16)) | id; 

    I2C1 -> CR2 |= I2C_CR2_START;
    if (I2C_CheckState() == 1) return;
    I2C1 -> TXDR = addr;
    if (I2C_CheckState() == 1) return;
    while (length){
        length--;
        if (I2C1 -> ISR & I2C_ISR_TCR){
            if (length > 255) I2C1 -> CR2 |= (((uint32_t)(255) << 16));
            else {
                I2C1 -> CR2 = I2C_CR2_AUTOEND | (((uint32_t)(length + 1) << 16)) | id; 
            }
        }
	
	I2C1 -> TXDR = byte;
	if (I2C_CheckState() == 1) return;
    }
    i = 0;
    while (I2C1 -> ISR & I2C_ISR_BUSY){
        if (i < 10000) i++;
        else return  1;
    }
}

void I2C_Init(){
    uint32_t i, j;
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;

    RCC -> APB1ENR |= RCC_APB1ENR_I2C1EN;

    GPIOA -> MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
    GPIOA -> OTYPER |= GPIO_OTYPER_OT_9 | GPIO_OTYPER_OT_10;
    GPIOA -> PUPDR |= GPIO_PUPDR_PUPDR9_0 | GPIO_PUPDR_PUPDR10_0;
    GPIOA -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9 | GPIO_OSPEEDER_OSPEEDR10;
    GPIOA -> AFR[1] |= 0x440;

    I2C1 -> CR1 = 0;
    //I2C1 -> TIMINGR = 0x1042130F; //100kHz
    I2C1 -> TIMINGR = 0x00300208;   //400kHz
    I2C1 -> CR1 = I2C_CR1_PE;

}